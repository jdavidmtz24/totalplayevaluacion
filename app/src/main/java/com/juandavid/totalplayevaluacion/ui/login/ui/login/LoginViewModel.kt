package com.juandavid.totalplayevaluacion.ui.login.ui.login

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import android.util.Patterns
import androidx.lifecycle.viewModelScope
import com.juandavid.totalplayevaluacion.R
import com.juandavid.totalplayevaluacion.data.model.LoginRequest
import com.juandavid.totalplayevaluacion.domain.GetLoginUsesCase
import com.juandavid.totalplayevaluacion.ui.login.data.LoginRepository
import com.juandavid.totalplayevaluacion.ui.login.data.Result
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class LoginViewModel @Inject constructor( var getLoginUsesCase:GetLoginUsesCase) : ViewModel() {

    private val _loginForm = MutableLiveData<LoginFormState>()
    val loginFormState: LiveData<LoginFormState> = _loginForm


    private val _loginResult = MutableLiveData<LoginResult>()
    val loginResult: LiveData<LoginResult> = _loginResult

    var session = MutableLiveData<String>()
    fun login(username: String, password: String) {

        if (isUserNameValid(username) && isPasswordValid(password)) {
            viewModelScope.launch {
                val resultLogin = getLoginUsesCase.invoke(LoginRequest(username,password))

                if (resultLogin != null) {
                    session.postValue(resultLogin.session)
                    _loginResult.value = LoginResult(success = LoggedInUserView(displayName = username))
                    Log.e("Session",resultLogin.session)
                }else{
                    _loginResult.value = LoginResult(error = R.string.login_failed)
                }
            }

        } else {
            _loginResult.value = LoginResult(error = R.string.login_failed)
        }
    }

    fun loginDataChanged(username: String, password: String) {
        if (!isUserNameValid(username)) {
            _loginForm.value = LoginFormState(usernameError = R.string.invalid_username)
        } else if (!isPasswordValid(password)) {
            _loginForm.value = LoginFormState(passwordError = R.string.invalid_password)
        } else {
            _loginForm.value = LoginFormState(isDataValid = true)
        }
    }

    private fun isUserNameValid(username: String): Boolean {
        return if (username.contains("@")) {
            Patterns.EMAIL_ADDRESS.matcher(username).matches()
        } else {
            username.isNotBlank()
        }
    }

    private fun isPasswordValid(password: String): Boolean {
        return password.length > 5
    }
}