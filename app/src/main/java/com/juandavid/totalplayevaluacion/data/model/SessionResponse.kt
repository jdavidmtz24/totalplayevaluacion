package com.juandavid.totalplayevaluacion.data.model

import com.google.gson.annotations.SerializedName

data class SessionResponse(@SerializedName("status") val status:Int,
                           @SerializedName("arrayReferences") val arrayReferences:ArrayList<ArrayReferences>)
