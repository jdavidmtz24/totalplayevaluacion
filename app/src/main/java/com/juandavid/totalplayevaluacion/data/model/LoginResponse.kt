package com.juandavid.totalplayevaluacion.data.model

import com.google.gson.annotations.SerializedName

data class LoginResponse(@SerializedName("session") val session:String)
