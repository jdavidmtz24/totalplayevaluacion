package com.juandavid.totalplayevaluacion.domain

import com.juandavid.totalplayevaluacion.data.TotalPlayRepository
import com.juandavid.totalplayevaluacion.data.model.LoginRequest
import com.juandavid.totalplayevaluacion.data.model.LoginResponse
import javax.inject.Inject

class GetLoginUsesCase @Inject constructor(private val repository:TotalPlayRepository)  {

    suspend operator fun invoke(loginRequest: LoginRequest):LoginResponse?= repository.postLogin(loginRequest)

}